# CANULAR - CAhier NUmérique pour LAboratoire de Recherche

_Un cahier de laboratoire électronique (ELN) interfacé avec la liseuse Bookeen Notéa_

Les cahiers de laboratoire électroniques (ELN) peuvent être un catalyseur crucial dans l’achèvement de l’ouverture et de la reproductibilité de la science.
Malgré une offre variée, un des freins actuels dans l’adoption des ELN est le manque de praticité dans l’usage au quotidien, notamment dû à la difficulté de prises des notes manuscrites.
Nous nous proposons de lever ce frein en interfaçant un ELN existant avec une liseuse électronique grand public

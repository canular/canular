package com.elab.elabtest;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import javax.net.ssl.HttpsURLConnection;


//import javax.json.stream.JsonGenerator;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        askPermissions();
        String endpoint="https://canular.inrae.fr/api/v1/";
        String key="9575536e036eb8674896df166046618ad61fc28b802d589ce78a0487ee029a33b47c07bad32a3640c3fe";//inra read only
        String key2="16d1b22c570c46f152841c7a63973eb86ab2c900fd85463dcf2401e9bca11816179728bebcc16ee7b277"; //inra read/write


        String path="/sdcard/Download/German-shepherd.jpg";
        String fichier="/sdcard/Download/api_android_test.txt";

        boolean status = checkInternetConnection();

        if(status){
            //String log = uploadFileText(endpoint, key2, 6, fichier);
            String log = uploadFile(endpoint, key2, 6, path);
            TextView tv = new TextView(this);
            tv.setText(log+"\n");
            setContentView(tv);

        }

        else{
            TextView tv = new TextView(this);
            tv.setText("Problème pas de réseau");
            setContentView(tv);
        }


    }


    // attachmentFileName = nom du fichier avec son extension , attachmentName que le nom du fichier sans extention
    @RequiresApi(api = Build.VERSION_CODES.O)
    private static String uploadFileText(String endpoint, String key, int id, String path_file ){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());
        StrictMode.setThreadPolicy(policy);
        String CRLF = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";
        endpoint = endpoint + "experiments/" + id;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        String charset = StandardCharsets.UTF_8.name();
        File file = new File(path_file);
        String attachmentName=file.getName();
        String attachmentFileName=file.getName();
        String log=" Test Upload fichier TEXT sous android\n";
        log=log+"File name "+attachmentName+"\n";
        String query=" ";
        try {
            // open a URL connection to the Servlet
            FileInputStream fileInputStream = new FileInputStream(path_file);
            URL url = new URL(endpoint);

            // Open a HTTP  connection to  the URL
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestProperty("Authorization", key);
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("files", attachmentFileName);
            log=log+"Connection\n";
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

//            dos.writeBytes(twoHyphens + boundary + crlf);
//            dos.writeBytes("Content-Disposition: form-data; name=\"files\";filename=\""
//                    + attachmentFileName + "\"" + crlf);
//
//            dos.writeBytes(crlf);

            OutputStream output = conn.getOutputStream();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);

            // Send text file.
            writer.append("--" + boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + file.getName() + "\"").append(CRLF);
            writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF); // Text file itself must be saved in this charset!
            writer.append(CRLF).flush();
            Files.copy(file.toPath(), output);
            output.flush(); // Important before continuing with writer!
            writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
            writer.append("--" + boundary + "--").append(CRLF).flush();


/*
            // create a buffer of  maximum size
            bytesAvailable = fileInputStream.available();

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0)
            {

                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(CRLF);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + CRLF);
*/

            // Responses from the server (code and message)
            int serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();

            log=log+"File name "+attachmentName+"\n";

            if(serverResponseCode == 200)
            {

                log=log+"OK "+serverResponseCode+"\n";
            }
            else{
                log=log+"NO "+serverResponseCode+"\n";
            }

            //close the streams //
            fileInputStream.close();
//            dos.flush();
//            dos.close();
            return log;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Malformed " + e.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return "HTML problem" + e.toString();
        }
    }


//    public static void testOther(String[] args) {
//        WebClient client = WebClient.builder().
//                baseUrl("https://api.ocrolus.com/v1/").
//                defaultHeaders(h -> {
//                    h.setBasicAuth("api-key", "api-secret");
//                    h.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//                }).
//                build();
//
//        MultipartBodyBuilder bookUploadBuilder = new MultipartBodyBuilder();
//        bookUploadBuilder.part("pk", 123456);
//        bookUploadBuilder.part("upload", new FileSystemResource("path/to/bank-statement.pdf"));
//
//        System.out.println(client.post().uri("book/upload").bodyValue(bookUploadBuilder.build()).
//                retrieve().bodyToMono(String.class).block());
//    }

    // attachmentFileName = nom du fichier avec son extension , attachmentName que le nom du fichier sans extention
    private static String uploadFile(String endpoint, String key, int id, String path_file ){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());
        StrictMode.setThreadPolicy(policy);
        String crlf = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";
        String charset = StandardCharsets.UTF_8.name();
        endpoint = endpoint + "experiments/" + id;
        File file = new File(path_file);
        String attachmentName=file.getName();
        String attachmentFileName=file.getName();
        String log=" Test Upload fichier sous android\n";
        log=log+"File name "+attachmentName+"\n";
        String query=" ";
        try {
            //Setup the request
            URL urlObj = new URL(endpoint);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestProperty("Authorization", key);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept-Charset", charset);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty(
                    "Content-Type", "multipart/form-data;boundary="+boundary);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.connect();
            log=log+"Connexion\n";

            query = String.format("file=%s",
                    URLEncoder.encode(attachmentName, charset));


            //Start content wrapper:
            DataOutputStream request = new DataOutputStream(conn.getOutputStream());

            request.writeBytes(twoHyphens + boundary + crlf);
            request.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""+attachmentName+"\"" + crlf);
            request.writeBytes(crlf);

            //image here
            InputStream in = null;
            in=file.toURI().toURL().openStream();
            FileInputStream filestream = new FileInputStream(path_file);
            //int length = file.length();
            log=log+"Length "+file.length()+"\n";
            //byte[] bytes = new byte[16*1024];
            //byte[] data_img = new byte[(int) file.length()];
            byte[] buffer = new byte[128];
            int size = -1;
            while (-1 != (size = filestream.read(buffer))) {
                request.write(buffer, 0, size);
            }

            // request.write(data-img);

            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary +
                    twoHyphens + crlf);

            //Flush output buffer:
            request.flush();
            request.close();
            log=log+"request"+request.toString()+"\n";
            if(conn.getResponseCode()==201 || conn.getResponseCode()==200)
            {
                return log+"CODE erreur OK\n"+conn.getResponseCode();
            }
            else{
                return log+"CODE erreur PAS OK\n"+conn.getResponseCode();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Malformed " + e.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return "HTML problem" + e.toString();
        }
    }


    //https://webdevdesigner.com/q/how-to-use-java-net-urlconnection-to-fire-and-handle-http-requests-16825/
    private static String update_experiment(String endpoint,String key, int id, String title, String body, String date) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String query=" ";
        try {
            System.out.println("Update expérimentation\n");
            endpoint = endpoint + "experiments/" + id;
            String charset = java.nio.charset.StandardCharsets.UTF_8.name();

             query = String.format("body=%s&date=%s&title=%s",
                    URLEncoder.encode(body, charset),
                    URLEncoder.encode(date, charset),
                    URLEncoder.encode(title, charset));


            URL url = new URL(endpoint);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestProperty("Authorization", key);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

            writer.write(query);
            writer.flush();
            String line;
            String responseZ = " ";
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                responseZ = responseZ + "\n" + line;
            }
            writer.close();
            reader.close();

            return "en cours : "+responseZ;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Malformed " + e.toString()+"\n"+query;

        } catch (Exception e) {
            e.printStackTrace();
            return "HTML problem" + e.toString()+"\n"+query;
        }
    }






    //export TOKEN=16d1b22c570c46f152841c7a63973eb86ab2c900fd85463dcf2401e9bca11816179728bebcc16ee7b277
    // curl -X POST -H "Authorization: $TOKEN" https://canular.inrae.fr/api/v1/experiments
    private String create_experiment(String endpoint,String key, int new_id){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try{
            endpoint = endpoint + "experiments/"+new_id;
            URL monURL = new URL(endpoint);
            HttpURLConnection http = (HttpURLConnection) monURL.openConnection();
            http.setRequestProperty("Authorization", key);
            http.setAllowUserInteraction(false);
            http.setInstanceFollowRedirects(true);
            http.setRequestMethod("POST");
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            http.setRequestProperty("Accept", "application/json");
            http.setRequestProperty("Content-Language", "en-US");
            http.setUseCaches(false);
            http.connect();
            if(http.getResponseCode()==201 || http.getResponseCode()==200)
            {
                return "OK, l'expérimentation a été créée\n";
            }
            else{
                return "l'expérimentation n'a pas été créée\n";
            }


        }catch (MalformedURLException e) {
            e.printStackTrace();
            return "Malformed "+e.toString();

        }catch (Exception e) {
            e.printStackTrace();
            return "HTML problem"+e.toString();
        }
    }

    private String get_id_last_experiment(String endpoint, String key){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            endpoint = endpoint + "experiments/";
            URL monURL = new URL(endpoint);
            HttpURLConnection http = (HttpURLConnection) monURL.openConnection();
            http.setRequestProperty("Authorization", key);
            http.setAllowUserInteraction(false);
            http.setInstanceFollowRedirects(true);
            http.setRequestMethod("GET");
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //http.setRequestProperty("Content-Type", "application/json; utf-8");
            http.setRequestProperty("Accept", "application/json");
            http.setRequestProperty("Content-Language", "en-US");
            http.setUseCaches(false);
            //http.setDoOutput(true);
            http.connect();
            if(http.getResponseCode()==201 || http.getResponseCode()==200)
            {
                try {
                    InputStream reader;
                    reader = http.getInputStream();
                    BufferedReader buffer;
                    buffer = new BufferedReader(new InputStreamReader(reader));
                    String ligne;
                    StringBuilder reponse = new StringBuilder();
                    while((ligne = buffer.readLine()) != null) {
                        reponse.append(ligne);
                        reponse.append("\n");
                        System.out.println(ligne);
                    }

                    buffer.close();
                    reader.close();
                    http.disconnect();
                    String finalReponse;
                    finalReponse=reponse.toString();
                    JSONArray mJsonArray = new JSONArray(finalReponse);
                    JSONObject mJsonObject = new JSONObject();
                    StringBuilder reponse2 = new StringBuilder();
                    int max_id=0;
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        mJsonObject = mJsonArray.getJSONObject(i);
                        reponse2.append(mJsonObject.getString("id")+"\n");
                        reponse2.append(mJsonObject.getString("title")+"\n");
                       // mJsonObject.getString("name");
                        int id=Integer.parseInt(mJsonObject.getString("id"));
                        if(id>max_id){max_id=id;}
                    }
                    //finalReponse=reponse2.toString();
                    //return finalReponse;
                    return String.valueOf(max_id);

                }catch (Exception e) {
                    String error = http.getErrorStream().toString();
                    Toast.makeText(this,error, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    return "Buffer pb  "+e.toString()+"\nError : "+error+"\n";

                }
            }
            else{
                int code = http.getResponseCode();
                return "Code erreur "+String.valueOf(code);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Malformed "+e.toString();

        }catch (Exception e) {
            e.printStackTrace();
            return "HTML problem"+e.toString();
        }

    }

    private boolean checkInternetConnection() {

        ConnectivityManager connManager =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();

        if (networkInfo == null) {
            Toast.makeText(this, "No default network is currently active", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!networkInfo.isConnected()) {
            Toast.makeText(this, "Network is not connected", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!networkInfo.isAvailable()) {
            Toast.makeText(this, "Network not available", Toast.LENGTH_LONG).show();
            return false;
        }
        Toast.makeText(this, "Network OK", Toast.LENGTH_LONG).show();
        return true;
    }

    @SuppressLint("SetTextI18n")
    protected String get_all_experiments(String endpoint, String key){
        TextView tv = new TextView(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            endpoint=endpoint+"experiments";
            URL monURL = new URL(endpoint);
            HttpURLConnection http=(HttpURLConnection)monURL.openConnection();
            http.setRequestProperty ("Authorization", key);
            http.setAllowUserInteraction(false);
            http.setInstanceFollowRedirects(true);
            http.setRequestMethod("GET");
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            http.setRequestProperty("Content-Language", "en-US");
            http.setUseCaches(false);
            http.connect();
            if(http.getResponseCode()==201 || http.getResponseCode()==200)
            {
                try {
                    InputStream reader;
                    reader = http.getInputStream();
                    BufferedReader buffer;
                    buffer = new BufferedReader(new InputStreamReader(reader));
                    String ligne;
                    StringBuilder reponse = new StringBuilder();
                    while((ligne = buffer.readLine()) != null) {
                        reponse.append(ligne);
                        reponse.append("\n");
                        System.out.println(ligne);
                    }

                    buffer.close();
                    buffer=null;
                    reader.close();
                    reader=null;
                    http.disconnect();
                    http=null;
                    String finalReponse;
                    finalReponse=reponse.toString();
                    //return "none";
                    return finalReponse;
                }catch (Exception e) {
                    String error = http.getErrorStream().toString();
                    Toast.makeText(this,error, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    return "Buffer pb  "+e.toString()+"\nError : "+error+"\n";

                }
            }
            else{
                int code = http.getResponseCode();
                return "Code erreur "+String.valueOf(code);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Malformed "+e.toString();
        }catch (Exception e) {
            e.printStackTrace();
            return "HTML problem"+e.toString();
        }

    }

    protected String get_one_experiment(String endpoint, String key, int id){
        TextView tv = new TextView(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            endpoint=endpoint+"experiments/"+id;
            URL monURL = new URL(endpoint);
            HttpURLConnection http=(HttpURLConnection)monURL.openConnection();
            http.setRequestProperty ("Authorization", key);
            http.setAllowUserInteraction(false);
            http.setInstanceFollowRedirects(true);
            http.setRequestMethod("GET");
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            http.setRequestProperty("Content-Language", "en-US");
            http.setUseCaches(false);
            http.connect();
            if(http.getResponseCode()==201 || http.getResponseCode()==200)
            {
                try {
                    InputStream reader;
                    reader = http.getInputStream();
                    BufferedReader buffer;
                    buffer = new BufferedReader(new InputStreamReader(reader));
                    String ligne;
                    StringBuilder reponse = new StringBuilder();
                    while((ligne = buffer.readLine()) != null) {
                        reponse.append(ligne);
                        reponse.append("\n");
                        System.out.println(ligne);
                    }

                    buffer.close();
                    buffer=null;
                    reader.close();
                    reader=null;
                    http.disconnect();
                    http=null;
                    String finalReponse;
                    finalReponse=reponse.toString();
                    //return "none";
                    return finalReponse;
                }catch (Exception e) {
                    String error = http.getErrorStream().toString();
                    Toast.makeText(this,error, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    return "Buffer pb  "+e.toString()+"\nError : "+error+"\n";

                }
            }
            else{
                int code = http.getResponseCode();
                return "Code erreur "+String.valueOf(code);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Malformed "+e.toString();
        }catch (Exception e) {
            e.printStackTrace();
            return "HTML problem"+e.toString();
        }

    }

    @TargetApi(23)
    protected  void askPermissions() {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }

    public static String parse(){
        String log = " ";
        //String filepath = Environment.getExternalStorageDirectory().getPath();
        String filepath="/data";
        log=log+"Filepath:"+filepath+"\n";
        File repertoire = new File(filepath);
        String liste[] = repertoire.list();
        String files=" ";
        if (liste != null) {
            log=log+"not nul";
            for (int i = 0; i < liste.length; i++) {
                files=files+"_"+i+"_"+liste[i].toString()+"\n";
            }
        }
        // return log+"\nListe"+liste.toString()+"\nFiles : "+files;
        return log+"\nLFiles : "+files;
    }

}

